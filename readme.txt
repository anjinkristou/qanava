Qanava v0.3.0
Copyright (C) 2005-2006-2007 Benoit AUTHEMAN
benoit@libqanava.org

LICENCE:

Qanava is distributed under the GNU LESSER GENERAL PUBLIC LICENSE (GNU LGPL v2.1)
Licence is detailed in the LICENCE.txt file.


BUILD:

For Windows 2000/XP: Open the build/vc8/qanava.sln Visual Studio 2005 solution file, then build the solution. For Visual Studio .net (msvc 2001), you have to re-generate all projects files by running 'qmake -t vclib' in all 'pro' and 'tests' subdirectories. Depending on what Qt distribution you are using, you might have to re-generate all project files to take into account your local QTDIR.

For Linux, just generate makefiles with the 'qmake -recursive' command, and launch the build with 'make'. The compilation has been tested successfully with GCC 4.1.0 and QT 4.2.2.

There are no dependencies outside of Trolltech QT.

Qanava is primarily developed on Windows 2000 with QT 4.2.2.


INSTALLATION:

There is currently no automated installation system, just use the 'qmake' command to generate makefiles, and gmake to build both the library and samples.


USING QANAVA / BUGS / PROBLEMS / etc.

For any problem related to Qanava, mail 'benoit@qanava.org' or post a comment on:
http://www.qanava.org/

If you did test Qanava, please send some feedback (even short!) by mail or via a comment.

I would also be interested in supporting the use of Qanava in an open source project, eventually adding specific features and dealing with problems that might occurs.


RELEASE NOTES:

v0.3.0: (07/12/29) After a one year freeze, Qanava has a new model view architecture that is no longer based on Interview (although an Interview interface is available). There is no new features available, as always, consider this release as highly unstable.

v0.0.10: (06/09/29) Qanava is now using QT 4.2.0 and the new GraphicsView framework. Custom nodes attributes can now be added to nodes trough the graph interface. A library was created as a repository for widgets used to display graph features, such as styles or nodes list. An abstract factory system now allows the registration of custom nodes to be displayed in the graph view. New samples were added to demonstrate styles modifications, and how to display custom nodes attributes (9/11 social graph).

v0.0.8b: (06/06/09) Serious bugs in GraphItemView and GraphItemModel were fixed. Better support for GCC compilation. Qanava now work with the latest KDE4 qt-copy (qt 4.1.3).

v0.0.8: (16/05/02) Qanava now support a full set of features (insertion/suppression/edition) to modify its graph, and all modification are reflected to and from to Interview model. Qanava is now distributed under the GNU LGPL.

v0.0.7: (06/04/02) Qanava no longer depend on QCanvas and does all its drawing as a standalone widget using Arthur. Model/view system now supports on the fly node insertion and suppression. Grid architecture has been cleaned, and the usual regular grid can now be changed with a check board one. The rendering code has received numerous optimisations and the UI is now much more reactive than with QCanvas code. This is the first version that is stable enough version to be considered useable as a library in "unstable" code.

v0.0.6: (06/03/11) Qanava now use QtCanvas (the qt4 port of Q3Canvas), and no longer require the qt3support library (do not forget to copy qtcanvas.cpp and qtcanvas.h from your QT installation to the src/can directory).

v0.0.5: (06/01/09) Qanava has the same canvas problems than in v0.0.4. Q3Canvas in Qt 4.1 is better than in Qt 4.0.1, but still far from being as fast and bug free than in Qt 3. This serious canvas problem cannot be addressed until Trolltech release their new canvas system in Qt 4.2.0.

v0.0.4: (05/11/29) While Qanava is now a qt4 library, the old QCanvas class is still used for graphic rendering until the new qt4 canvas system is released with qt4.1 (even if there is no such system be in the preview release, hum...). The support for the old canvas in qt 4.0.1 is extremely slow, and seems to be seriously buggy (clipping and zoom problems). The X11 version of Q3Canvas seems to be less affected than win32, anyways, Qanava v0.0.4 should be considered a transition release until the display 'backend' is stabilized (either using Arthur, or hopefully a new graphic canvas).


NO WARRANTY

  BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO
WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR
OTHER PARTIES PROVIDE THE LIBRARY "AS IS" WITHOUT WARRANTY OF ANY
KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME
THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

