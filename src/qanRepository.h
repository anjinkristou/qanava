/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanRepository.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 December 23
//-----------------------------------------------------------------------------


#ifndef qanRepository_h
#define qanRepository_h


// Qanava headers
#include "./qanConfig.h"
#include "./qanGraph.h"


// STD headers
#include <string>


// QT headers
#include <QDomDocument>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

		//! Model an abstract storage object for graphs.
		/*!
			\nosubgrouping
		*/
		class Repository
		{
			/*! \name Graph Serialization Management *///----------------------
			//@{
		public:

			//! Repository constructor with name initialization.
			Repository( const std::string& name ) : _name( name ) { }

			//! Repsoitory standard virtual destructor.
			virtual	~Repository( ) { }

			//! .
			virtual	void		load( Graph* graph ) = 0;

			//! .
			virtual	void		save( Graph* graph ) = 0;

			//! Get the repository name.
			const std::string&	getName( ) const { return _name; }

		private:

			//! Repository name (usually a file name).
			std::string		_name;
			//@}
			//-----------------------------------------------------------------
		};



		//!
		/*!
			\nosubgrouping
		*/
		class PajekRepository : public Repository
		{
			/*! \name Graph Serialization Management *///----------------------
			//@{
		public:

			PajekRepository( const std::string& name ) : Repository( name ) { }

			virtual	~PajekRepository( ) { }

			//! .
			virtual	void	load( Graph* graph );

			//! .
			virtual	void	save( Graph* graph );

		private:

			//! Current data mode (Pajek Vertices, Arcs or Edges)
			enum Mode
			{
				UNDEFINED		= 1,
				VERTICES		= 2,
				ARCS			= 4,
				EDGES			= 8
			};
			//@}
			//-----------------------------------------------------------------
		};



		//! DEPRECATED
		/*! DEPRECATED, used for debugging purposes only.
			\nosubgrouping
		*/
		class GraphvizRepository : public Repository
		{
			/*! \name Graph Serialization Management *///----------------------
			//@{
		public:

			GraphvizRepository( const std::string& name ) : Repository( name ) { }

			virtual	~GraphvizRepository( ) { }

			//! .
			virtual	void	load( Graph* graph );

			//! .
			virtual	void	save( Graph* graph );
			//@}
			//-----------------------------------------------------------------
		};

		//! GML (XML based Graph Markup Language) repository.
		/*!
			\nosubgrouping
		*/
		class GMLRepository : public Repository
		{
			/*! \name Graph Serialization Management *///----------------------
			//@{
		public:

			GMLRepository( const std::string& name );

			virtual	~GMLRepository( ) { }

			//! .
			virtual	void	load( Graph* graph );

			//! .
			virtual	void	save( Graph* graph );

		private:

			void			parseGraph( QDomDocument domDocument, QDomElement graphChild, Graph* graph );

			qan::Style*		parseStyle( QDomElement& node );
			//@}
			//-----------------------------------------------------------------
		};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qanRepository_h

