/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canGraphModel.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 December 27
//-----------------------------------------------------------------------------


#ifndef canGraphModel_h
#define canGraphModel_h


// Qanava headers
#include "./qanNodeItem.h"
#include "./qanEdgeItem.h"


// QT headers
#include <QStandardItemModel>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	//! Standard interface for template listeners used in qan::GraphT.
	class GraphListener
	{
		/*! \name Graph Topology Listener *///---------------------------------
		//@{
	public:

		void	init( Node::List& rootNodes ) { }

		void	edgeInserted( qan::Edge& edge ) { }

		void	edgeRemoved( qan::Edge& edge ) { }

		void	nodeInserted( qan::Node& node ) { }

		void	nodeRemoved( qan::Node& node ) { }

		void	nodeChanged( qan::Node& node ) { }
		//@}
		//---------------------------------------------------------------------
	};


	//! Expose a graph data structure to a QT Interview model.
	/*!
		\nosubgrouping
	*/
	class GraphModel : public QStandardItemModel, public GraphListener
	{
	public:

		GraphModel( );

		/*! \name Scene Interface Management *///------------------------------
		//@{
	public:

		qan::Node*	getModelIndexNode( QModelIndex index );

		void		init( Node::List& rootNodes );

		void		edgeInserted( qan::Edge& edge );

		void		edgeRemoved( qan::Edge& edge );

		void		nodeInserted( qan::Node& node );

		void		nodeRemoved( qan::Node& node );

		void		nodeChanged( qan::Node& node );

	protected:

		void			visit( qan::Node& node );

		void			createNode( qan::Node& node );

		void			removeNode( qan::Node& node );

		QStandardItem*	addItem( qan::Node& node, QStandardItem* parent = 0 );

		void			removeItem( qan::Node& node );

		//! Remove an item and all its child from the model.
		void			removeItemHierarchy( QStandardItem* item );

		//! Add and item (and all its childs) to a given parent.
		void			addItemHierarchy( qan::Node& node, QStandardItem* parent = 0, qan::Node::Set* marked = 0 );

		typedef			QMultiMap< qan::Node*, QStandardItem* >	NodeItemMap;

		NodeItemMap		_nodeItemMap;
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // canGraphModel_h

