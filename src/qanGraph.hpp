/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laGraph.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 February 15
//-----------------------------------------------------------------------------

// Qt headers
#include <QDateTime>


namespace qan { // ::qan


/* GraphT Constructor/Destructor *///-------------------------------------------
template < class M, class O >
GraphT< M, O >::GraphT( ) :
	_m( ), _o( )
{
	_m.init( _rootNodes );
	_o.init( _rootNodes );

	addAttribute< int >( "Type", 0 );
	addAttribute< QString >( "Label", QString( "" ) );
	addAttribute< VectorF >( "Position", VectorF( 2 ) );
	addAttribute< VectorF >( "Dimension", VectorF( 2 ) );
	addAttribute< QDateTime >( "Date", QDateTime( ) );
}

template < class M, class O >
GraphT< M, O >::~GraphT( )
{
	clear( );
}
//-----------------------------------------------------------------------------


template < class M, class O >
void	GraphT< M, O >::modifyNode( Node& node, const QString& name, int type, void* object )
{
	node.setLabel( name );

	if ( type != -1 )
		node.setType( type );

	if ( object != 0 )
		setNodeObject( node, object );

	_m.nodeChanged( node );
	_o.nodeChanged( node );
}

template < class M, class O >
void	GraphT< M, O >::setNodeObject( Node& node, void* object )
{
	// Map the registered node to a given memory object that act as a key
	if ( object != 0 )
	{
		_objectNodeMap.insert( object, &node );
		_nodeObjectMap.insert( &node, object );
	}
}



/* Edge/Node Management *///---------------------------------------------------
template < class M, class O >
void	GraphT< M, O >::addNode( Node* node, void* object )
{
	assert( node != 0 );
	initNodeAttributes( *node );

	_nodes.push_back( node );
	_nodesSet.insert( node );

	if ( object != 0 )
		setNodeObject( *node, object );

	if ( node->getInDegree( ) == 0 )
		addRootNode( *node );
}

template < class M, class O >
Node*	GraphT< M, O >::addNode( const QString& name, int type, void* object )
{
	Node* node = new Node( name, type );
	addNode( node, object );
	return node;
}

template < class M, class O >
void	GraphT< M, O >::insertNode( Node* node, void* object )
{
	assert( node != 0 );
	addNode( node, object );
	_m.nodeInserted( *node );
	_o.nodeInserted( *node );
}

template < class M, class O >
Node*	GraphT< M, O >::insertNode( const QString& name, int type, void* object )
{
	Node* node = new Node( name, type );
	insertNode( node, object );
	return node;
}

template < class M, class O >
void	GraphT< M, O >::removeNode( Node& node )
{
	// Test if node subnodes will eventually became root nodes after their super node removal
	Node::List rootNodes;
	Node::List outNodes; node.collectOutNodes( outNodes );
	for ( Node::List::iterator outNodeIter = outNodes.begin( ); outNodeIter != outNodes.end( ); outNodeIter++ )
	{
		Node* outNode = *outNodeIter;
		if ( !isRootNode( *outNode ) && outNode->getInDegree( ) <= 1 )
			rootNodes.push_back( outNode );
	}

	// Add orphan out nodes as root node (node is removed to force view update and maintain graph coherency for listeners)
	// Oprhan out nodes are removed before 'node' so that they still have a valid parent in
	// an eventual associed view (for example in a QAbstractItem view, removing node would cause
	// the model to have a gap preventing node out nodes to be destroyed).
	for ( Node::List::iterator rootNodeIter = rootNodes.begin( ); rootNodeIter != rootNodes.end( ); rootNodeIter++ )
	{
		Node& rootNode = **rootNodeIter;
		//_m.nodeInsertedBegin( rootNode );
		addRootNode( rootNode );
		_m.nodeInserted( rootNode );
		_o.nodeInserted( rootNode );
	}


	//_m.nodeRemovedBegin( node );

	// Disconnect node from its "in edges".
	Edge::List::iterator inEdgeIter = node.getInEdges( ).begin( );
	for ( ; inEdgeIter != node.getInEdges( ).end( ); inEdgeIter++ )
	{
		Edge& inEdge = ( **inEdgeIter );
		inEdge.getSrc( ).getOutEdges( ).removeAll( &inEdge );
	}
	node.getInEdges( ).clear( );

	// Disconnect node from its "out edges".
	Edge::List::iterator outEdgeIter = node.getOutEdges( ).begin( );
	for ( ; outEdgeIter != node.getOutEdges( ).end( ); outEdgeIter++ )
	{
		Edge& outEdge = ( **outEdgeIter );
		outEdge.getDst( ).getInEdges( ).removeAll( &outEdge );
	}
	node.getOutEdges( ).clear( );

	// Remove node from the graph various node list
	_nodes.removeAll( &node );
	_nodesSet.remove( &node );
	void* object = findObject( &node );
	if ( object != 0 )
		_objectNodeMap.remove( object );
	_nodeObjectMap.remove( &node );
	removeRootNode( node );

	_m.nodeRemoved( node );
	_o.nodeRemoved( node );
}

template < class M, class O >
void	GraphT< M, O >::addEdge( Edge* edge )
{
	assert( edge != 0 );
	if ( edge->hasSrc( ) )
		edge->getSrc( ).addOutEdge( *edge );
	if ( edge->hasDst( ) )
	{
		edge->getDst( ).addInEdge( *edge );
		removeRootNode( edge->getDst( ) );   // Dst can't be a root node, supress it to maintain coherency
	}
	_edges.push_back( edge );
}

template < class M, class O >
Edge*	GraphT< M, O >::addEdge( Node& a, Node& b, float weight )
{
	Edge* edge = new Edge( &a, &b, weight );
	addEdge( edge );
	return edge;
}


template < class M, class O >
void	GraphT< M, O >::insertEdge( Edge* edge )
{

}

template < class M, class O >
Edge*	GraphT< M, O >::insertEdge( Node& src, Node& dst, float weight )
{
	// Save destination node topology.
/*	Edge::List inEdges;
	std::copy( dst.getInEdges( ).begin( ), dst.getInEdges( ).end( ), std::back_insert_iterator< Edge::List >( inEdges ) );
	Edge::List outEdges;
	std::copy( dst.getOutEdges( ).begin( ), dst.getOutEdges( ).end( ), std::back_insert_iterator< Edge::List >( outEdges ) );

	// Delete the node to force view update (and eventually move the node to its correct position)
	void* object = findObject( &dst );
	removeNode( dst );

	// Restore destination node in and out edges (and reconnect theses edges to their extremity node).
	for ( Edge::List::iterator inEdgeIter = inEdges.begin( ); inEdgeIter != inEdges.end( ); inEdgeIter++ )
	{
		Edge* inEdge = *inEdgeIter;
		dst.addInEdge( *inEdge );
		inEdge->getSrc( ).addOutEdge( *inEdge );
	}
	for ( Edge::List::iterator outEdgeIter = outEdges.begin( ); outEdgeIter != outEdges.end( ); outEdgeIter++ )
	{
		Edge* outEdge = *outEdgeIter;
		dst.addOutEdge( *outEdge );
		outEdge->getDst( ).addInEdge( *outEdge );

		// Remove (eventually) out node from root node
		removeRootNode( outEdge->getDst( ) );
	}*/

	// Insert back the node to its new correct view position
	Edge* edge = addEdge( src, dst, weight );
	//insertNode( &dst, object );
	_m.edgeInserted( *edge );
	_o.edgeInserted( *edge );
	return edge;
}

template < class M, class O >
Edge*	GraphT< M, O >::insertNode( Node* node, Node& superNode )
{
	return 0;
}

/*!
	\return	true is there is an edge between a and b (whatever is orientation is), false otherwise.
 */
template < class M, class O >
bool	GraphT< M, O >::hasEdge( Node& a, Node& b ) const
{
	Node::Set adjacentA;
	a.getAdjacentNodesSet( adjacentA );
	if ( adjacentA.find( &b ) != adjacentA.end( ) )
		return true;

	Node::Set adjacentB;
	b.getAdjacentNodesSet( adjacentB );
	if ( adjacentB.find( &a ) != adjacentB.end( ) )
		return true;

	return false;
}

template < class M, class O >
void	GraphT< M, O >::updateModels( )
{
	_m.init( _rootNodes );
	_o.init( _rootNodes );
}

/*!
	\return	a pointer on the node of request label. 0 if no such node exists.
*/
template < class M, class O >
Node*	GraphT< M, O >::findNode( const QString& label )
{
	for ( Node::List::iterator nodeIter = _nodes.begin( ); nodeIter != _nodes.end( ); nodeIter++ )
	{
		Node* node = *nodeIter;
		if ( node->getLabel( ) == label )
			return node;
	}
	return 0;
}

template < class M, class O >
Node*	GraphT< M, O >::findNode( void* object )
{
	return _objectNodeMap.value( object, 0 );
}

template < class M, class O >
Node*	GraphT< M, O >::findNode( int node )
{
	Node::List::iterator nodeIter = _nodes.begin( );
	int n = 0;
	while ( n < node )
	{
		nodeIter++;
		n++;
	}
	if ( nodeIter != _nodes.end( ) )
		return *nodeIter;
	return 0;
}

/*!
	\result	The node index if node is registered in this graph, -1 if there is no node or index.
 */
template < class M, class O >
int		GraphT< M, O >::findNode( const Node& node ) const
{
	Node::List::const_iterator nodeIter = _nodes.begin( );
	int n = 0;
	while ( nodeIter != _nodes.end( ) && ( *nodeIter != &node ) )
	{
		nodeIter++;
		n++;
	}
	if ( nodeIter != _nodes.end( ) )
		return n;
	return -1;
}

template < class M, class O >
void*	GraphT< M, O >::findObject( Node* node )
{
	NodeObjectMap::iterator objectIter = _nodeObjectMap.find( node );
	return ( objectIter != _nodeObjectMap.end( ) ? objectIter.value( ) : 0 );
}

/*!
	\param	node	Node to be searched in this graph.
	\return			true if the node is found, false otherwise.
 */
template < class M, class O >
bool	GraphT< M, O >::hasNode( Node* node ) const
{
	return ( _nodesSet.find( node ) != _nodesSet.end( ) );
}

template < class M, class O >
void	GraphT< M, O >::collectNodes( Node::Set& nodes ) const
{
	nodes.reserve( _nodes.size( ) );
	foreach ( Node* node, _nodes )
		nodes.insert( node );
	// FIXME
	/*std::copy( _nodes.begin( ), _nodes.end( ),
			   std::insert_iterator< Node::Set >( nodes, nodes.begin( ) ) );*/
}

/*! This method clear the nodes, the fast node search cache, the node object mapping
    system and the edge list. Registered nodes and edges are not only dereferenced, but
	destroyed with a call to delete.
 */
template < class M, class O >
void		GraphT< M, O >::clear( )
{
	_objectNodeMap.clear( );
	_nodeObjectMap.clear( );

	for ( Edge::List::iterator edgeIter = _edges.begin( ); edgeIter != _edges.end( ); edgeIter++ )
		delete *edgeIter;
	_edges.clear( );

	_nodesSet.clear( );
	for ( Node::List::iterator nodeIter = _nodes.begin( ); nodeIter != _nodes.end( ); nodeIter++ )
		delete *nodeIter;
	_nodes.clear( );


    _rootNodes.clear( );
    _rootNodesSet.clear( );
}
//-----------------------------------------------------------------------------


/* Root Node Management *///---------------------------------------------------
/*!
	Root nodes manually inserted via addRoot() are cleared from the root nodes (and eventually
	automatically re-added if needed).
 */
template < class M, class O >
void	GraphT< M, O >::generateRootNodes( )
{
	_rootNodes.clear( );
	for ( Node::List::iterator nodeIter = _nodes.begin( ); nodeIter != _nodes.end( ); nodeIter++ )
	{
		if ( ( *nodeIter )->getInDegree( ) == 0 )
			addRootNode( **nodeIter );
	}
}

/*!
	\param	node	node that must be tested against the root nodes set (must be a graph node).
	\return			true if the node is a graph root node, false otherwise.
 */
template < class M, class O >
bool	GraphT< M, O >::isRootNode( const Node& node ) const
{
	return ( _rootNodesSet.find( const_cast< Node* >( &node ) ) != _rootNodesSet.end( ) );
}
//-----------------------------------------------------------------------------


} // ::qan

