/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanLayout.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 May 22
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanLayout.h"
#include "./qanSimpleLayout.h"


// Std headers
#include <vector>


namespace qan { // ::qan


/* Layout Generation Management *///-------------------------------------------
void	Layout::resetNodesPositions( Graph& graph )
{
	Node::List& nodes = graph.getNodes( );
	for ( Node::List::iterator nodeIter = nodes.begin( ); nodeIter != nodes.end( ); nodeIter++ )
		( *nodeIter )->setPosition( -1.0f, -1.0f );
}
//-----------------------------------------------------------------------------



/* Random Layout Generation Management *///------------------------------------
void	Random::layout( Graph& graph, QGraphicsScene* scene, QRectF r, QProgressDialog* progress, int step )
{
	if ( progress != 0 )
	{
		progress->setMaximum( graph.getNodeCount( ) );
		progress->setValue( 0 );
	}

	srand( QDateTime::currentDateTime( ).toTime_t( ) );

	Node::List& nodes = graph.getNodes( );
	for ( Node::List::iterator nodeIter = nodes.begin( ); nodeIter != nodes.end( ); nodeIter++ )
	{
		Node* node = *nodeIter;

		float w = ( float )( ( rand( ) % 100 ) / 100.0 * r.width( ) );
		float h = ( float )( ( rand( ) % 100 ) / 100.0 * r.height( ) );
		node->setPosition( w, h );

		if ( progress != 0 )
			progress->setValue( progress->value( ) + 1 );
	}

	if ( progress != 0 )
		progress->close( );
}
//-----------------------------------------------------------------------------


/* Force Layout Generation Management *///-------------------------------------
/*! Initial positions will be generated using a fixed layout (random or colimacon) if step is
	superior of 1.
 */
void	UndirectedGraph::layout( Graph& graph, QGraphicsScene* scene, QRectF r, QProgressDialog* progress, int step )
{
	int runCount = 50;
	if ( step != -1 )
		runCount = step;
	if ( progress != 0 )
	{
		progress->setMaximum( runCount );
		progress->setValue( 0 );
	}

	// Generate a random layout for algorithm initialisation
	if ( runCount > 1 )
	{
		//Random initalLayout;
		Colimacon initalLayout;
		initalLayout.layout( graph, scene, r );
	}

	// Apply the spring force algorithm
	std::vector< VectorF > positions;
	positions.reserve( graph.getNodeCount( ) + 1 );  // Last position reserved for a virtual center node
	unsigned int n = 0;
	for ( ; n < graph.getNodeCount( ) + 1; n++ )
		positions.push_back( VectorF( 2 ) );
	Node::Set nodesSet; graph.collectNodes( nodesSet );

	_center.setPosition( r.width( ) / 2, r.height( ) / 2 );
	//_center.setPosition( 100., 100. );
	Node::List& nodes = graph.getNodes( );
	for ( int iter = 0; iter < runCount; iter++ )
	{
		double modification = 0.;

		// Compute new nodes positions using the spring embedder model
		Node::List::iterator nodeIter = nodes.begin( );
		for ( n = 0; nodeIter != nodes.end( ); n++, nodeIter++ )
		{
			Node& node = **nodeIter;

			Node::Set adjacentNodes;
			node.getAdjacentNodesSet( adjacentNodes );
			if ( graph.isRootNode( node ) )
				adjacentNodes.insert( &_center );
			VectorF fspring = computeSpringForce( node, adjacentNodes, graph );
			VectorF frep = computeRepulseForce( node, adjacentNodes, nodesSet, graph );

			VectorF delta( 2 );
			delta = ( frep + fspring ) / ( float )( nodes.size( ) + 1.f );
			positions[ n ] = node.getPosition( ) + delta;
			modification += length2( delta );
		}

		// Apply modifications for a virtual center node connected to all root nodes
		{
			n = nodes.size( );
			VectorF fspring = computeSpringForce( _center, graph.getRootNodesSet( ), graph );
			VectorF frep = computeRepulseForce( _center, graph.getRootNodesSet( ), nodesSet, graph );

			VectorF delta( 2 );
			delta = ( frep + fspring ) / ( float )( nodes.size( ) + 1.f );
			positions[ n ] = _center.getPosition( ) + delta;
		}

		// Move the nodes to their new positions
		int p = 0;
		for ( nodeIter = nodes.begin( ); nodeIter != nodes.end( ); nodeIter++, p++ )
			( *nodeIter )->setPosition( positions[ p ] );
		_center.setPosition( positions[ nodes.size( ) ] );

		// Stop iterating if node have converged to a fixed position
		if ( modification < ( 5. * nodes.size( ) ) )
			break;

		// Update progress bar
		if ( progress != 0 )
			progress->setValue( iter );

		// Detect process interruption
		if ( progress != 0 && progress->wasCanceled( ) )
			break;
	}

	if ( progress != 0 )
		progress->close( );
}

VectorF	UndirectedGraph::computeRepulseForce( Node& u, Node::Set& adjacentNodes, Node::Set& nodes, Graph& graph )
{
	VectorF force( 2 );
	force( 0 ) = force( 1 ) = 0.f;

	// Compute repulsion forces for all non adjacent nodes
	Node::Set nonAdjacentNodes( nodes );
	nonAdjacentNodes.subtract( adjacentNodes );
	if ( !graph.isRootNode( u ) )
		nonAdjacentNodes.insert( &_center );

	Node::Set::iterator nonAdjacentNodeIter = nonAdjacentNodes.begin( );
	for ( ; nonAdjacentNodeIter != nonAdjacentNodes.end( ); nonAdjacentNodeIter++ )
	{
		Node& v = **nonAdjacentNodeIter;
		if ( &v == &u )
			continue;

		// Compute repulsion forces between item and
		VectorF& pu = u.getPosition( );
		VectorF& pv = v.getPosition( );
		VectorF uv = pv - pu;
		uv *= - ( 80 * 80 ) / ( 1.0 + length2( uv ) );
		force += uv;
	}
	return force;
}

VectorF	UndirectedGraph::computeSpringForce( Node& u, Node::Set& adjacentNodes, Graph& graph )
{
	VectorF force( 2 );
	force( 0 ) = 0.f; force( 1 ) = 0.f;

	// Compute attraction forces for all adjacent nodes (roots nodes are considered adjacents)
	Node::Set::iterator adjacentNodeIter = adjacentNodes.begin( );
	for ( ; adjacentNodeIter != adjacentNodes.end( ); adjacentNodeIter++ )
	{
		Node& v = **adjacentNodeIter;

		// Compute attraction forces between item and
		VectorF& pu = u.getPosition( );
		VectorF& pv = v.getPosition( );
		VectorF uv = pv - pu;

		double l = length( uv ) / 100.f;
		double size = 1.;
		if ( l > 1.0 )
			size = 2.f * log( l );

		uv *= size;
		force += uv;
	}

	return force;
}

float		UndirectedGraph::length( const VectorF& v )
{
	return sqrt( ( v( 0 ) * v( 0 ) ) + ( v( 1 ) * v( 1 ) ) );
}

float		UndirectedGraph::length2( const VectorF& v )
{
	return ( v( 0 ) * v( 0 ) ) + ( v( 1 ) * v( 1 ) );
}
//-----------------------------------------------------------------------------


} // ::qan
