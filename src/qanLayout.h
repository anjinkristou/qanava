/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanLayout.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 May 22
//-----------------------------------------------------------------------------


#ifndef qanLayout_h
#define qanLayout_h


// Qanava headers
#include "./qanGraph.h"
#include "./qanGridItem.h"


// STD headers
#include <vector>


// QT headers
#include <QRectF>
#include <QProgressDialog>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	//! Abstract interface defining a graph layout algorithm (placement of node in space).
	/*!
		\nosubgrouping
	*/
	class	Layout
	{
	public:

		/*! \name Layout Constructor/Destructor *///---------------------------
		//@{
		//! Layout constructor.
		Layout( ) { }

		//! Layout virtual destructor.
		virtual ~Layout( ) { }

	private:

		Layout( const Layout& l );
		//@}
		//---------------------------------------------------------------------



		/*! \name Layout Generation Management *///----------------------------
		//@{
	public:

		//! Layout nodes from a given graph using r as a clipping rect, and update grid.
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 ) = 0;

	protected:

		//! Reset all nodes positions.
		static void		resetNodesPositions( Graph& graph );
		//@}
		//---------------------------------------------------------------------
	};



	//! Randomly layout an undirected graph.
	/*!
		\nosubgrouping
	*/
	class Random : public Layout
	{
		/*! \name Random Constructor/Destructor *///---------------------------
		//@{
	public:

		//! Random constructor.
		Random( ) : Layout( ) { }
		//@}
		//---------------------------------------------------------------------



		/*! \name Random Layout Generation Management *///---------------------
		//@{
	public:

		//! .
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 );
		//@}
		//---------------------------------------------------------------------
	};



	//! Layout an undirected graph using a spring force algorithm.
	/*!
		\nosubgrouping
	*/
	class UndirectedGraph : public Layout
	{
		/*! \name UndirectedGraph Constructor/Destructor *///------------------
		//@{
	public:

		//! UndirectedGraph constructor.
		UndirectedGraph( ) : Layout( ), _center( "" ) { }
		//@}
		//---------------------------------------------------------------------



		/*! \name Force Layout Generation Management *///----------------------
		//@{
	public:

		//! Layout 'graph' using a spring force algorithm.
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 );

		static void		add( VectorF& a, const VectorF& b );

		static void		scale( VectorF& p, float scale );

		static VectorF	vector( VectorF a, VectorF b );

		static float	length( const VectorF& v );

		static float	length2( const VectorF& v );

	private:

		VectorF	computeRepulseForce( Node& node, Node::Set& adjacentNodes, Node::Set& nodes, Graph& graph );

		VectorF	computeSpringForce( Node& node, Node::Set& adjacentNodes, Graph& graph );

		Node		_center;
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qanLayout_h

