/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qarte software.
//
// \file	qanStyle.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 January 03
//-----------------------------------------------------------------------------


#ifndef qan_can_Style_h
#define qan_can_Style_h


// QT headers
#include <QIcon>
#include <QVariant>
#include <QList>
#include <QMap>
#include <QAbstractListModel>


//-----------------------------------------------------------------------------
namespace qan // ::qan
{
		//! Specify graphic and other attributes for a specific primitive (usually a Canvas Item).
		/*!
			\nosubgrouping
		*/
		class Style : public QAbstractListModel
		{
			Q_OBJECT

		public:

			//! Manage styles for a set of objects (usually graphics items).
			/*!
				If you are applying style to nodes, it is usually safer to use the dedicated methods
				from GraphScene such as applyStyle() rather than accessing this style manager
				directly.

				\sa GraphScene
				\nosubgrouping
			*/
			class Manager
			{
				/*! \name Manager Constructor/Destructor *///------------------
				//@{
			public:

				Manager( );

				virtual ~Manager( );
				//@}
				//-------------------------------------------------------------



				/*! \name Style Management *///--------------------------------
				//@{
			public:

				//! Clear this manager from all mapping and delete registered styles.
				void			clear( );

				//! Set the style for a specific object.
				void			setStyle( void* object, Style& style );

				//! Set the style for a specific node type.
				void			setStyle( int type, Style& style );

				//! Get the style for a specific object.
				Style*			getStyle( void* object );

				//! Get the style for a specific object.
				const Style*	getStyle( const void* object ) const;

				//! Get the style for a specific node type.
				Style*			getStyle( int type );

				//! Get the style for a specific node type.
				const Style*	getStyle( int type ) const;

				//! Get a void style (empty style is owned by this manager, but not registered in).
				Style*			getEmptyStyle( ) const { return _empty; }

			protected:

				//! Map style to their target object.
				typedef QMap< void*, Style* >	ObjectStyleMap;

				//! Map style to their target object.
				typedef QMap< int, Style* >		TypeStyleMap;

				//! .
				ObjectStyleMap		_objectStyleMap;

				//! .
				TypeStyleMap		_typeStyleMap;

				Style*				_empty;
				//@}
				//-------------------------------------------------------------



				/*! \name Image Caching Management *///------------------------
				//@{
			public:

				//! Get an image from its file name or its cached version the second call of the same filename.
				QImage			getImage( QString fileName );

				//! Clear this image manager mappings and data (images).
				void			clearImages( );

			protected:

				typedef QMap< QString, QImage >	NameImageMap;

				//! Map image filename to concrete QT images.
				NameImageMap	_nameImageMap;
				//@}
				//-------------------------------------------------------------
			};


			/*! \name Style Constructor/Destructor *///------------------------
			//@{
		public:

			//! Style constructor.
			Style( ) : _name( "" ) { }

			//! Style constructor with name initialisation.
			Style( QString name ) : _name( name ) { }

		private:

			//! Style empty private copy constructor.
			Style( const Style& style );
			//@}
			//-----------------------------------------------------------------



			/*! \name Style Name Management *///-------------------------------
			//@{
		public:

			//! Get the style name.
			QString			getName( ) const { return _name; }

		protected:

			//! Style name.
			QString			_name;
			//@}
			//-----------------------------------------------------------------



			/*! \name Attribute Management *///--------------------------------
			//@{
		public:

			//! Add an attribute and register its name.
			template < typename T >
			void		addT( QString name, T value )
			{
				add( name, QVariant( value ) );
			}

			template < typename T >
			T			getT( QString name ) const
			{
				QVariant value = get( name );
				if ( value.isValid( ) )
					return value.value< T >( );
				return T( );
			}

			//! Return the number of attributes/value couples registered in this style.
			unsigned int	size( ) const { return _nameValueMap.size( ); }

			//! Add an attribute and register its name.
			void			add( QString name, QVariant value );

			//! Remove a style attribute by name.
			void			remove( QString name );

			//! Change an attribute name (changing to an already existing name, has no effect).
			bool			rename( QString name, QString newName );

			//! Test if an attribute with a specific name has been set.
			bool			has( QString name ) const;

			//! Get an attribute by name (invalide QVariant is returned if the name does not exists).
			QVariant		get( QString name );

			//! Get an attribute by name (invalide QVariant is returned if the name does not exists).
			const QVariant	get( QString name ) const;

			//! Specialized version of add() for QColor objects.
			void			addColor( QString name, int r, int g, int b );

			//! Get a previously registered QColor by name.
			QColor			getColor( QString name ) const;

			//! Specialized version of add() for QIcon objects.
			void			addIcon( QString name, QIcon& icon );

			//! Get a previously registered QIcon by name.
			QIcon			getIcon( QString name ) const;

			//! Specialized version of add() for QImage objects.
			void			addImage( QString name, QImage image );

			//! Add an image attribute, image loading is delayed until display, and images shared accross styles.
			void			addImageName( QString name, QString fileName );

			//! Test if an image name with a specific name has been set.
			bool			hasImageName( QString name ) const;

			//! .
			QString			getImageName( QString name ) const;

		protected:

			typedef	QMap< QString, QVariant >	NameValueMap;

			bool				isImageName( QString name ) { return _imageNames.contains( name ); }

			QList< QString >	_imageNames;

			//! .
			NameValueMap		_nameValueMap;
			//@}
			//-----------------------------------------------------------------


			/*! \name Qt Model Interface *///----------------------------------
			//@{
		public:

			//! Define the article attribute offset (row) in table.
			enum AttributeOffset
			{
				TITLE	= 0,
				AUTHOR	= 1,
				SOURCE	= 2,
				SUB		= 3,
				SUP		= 4
			};

			virtual QVariant		data( const QModelIndex &index, int role ) const;

			virtual	bool			setData( const QModelIndex& index, const QVariant& value, int role = Qt::EditRole );

			virtual Qt::ItemFlags	flags( const QModelIndex &index ) const;

			virtual QVariant		headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

			virtual int				rowCount( const QModelIndex& parent = QModelIndex( ) ) const;

			virtual int				columnCount( const QModelIndex& parent = QModelIndex( ) ) const;
			//@}
			//-----------------------------------------------------------------


			/*! \name Model Signals Management *///----------------------------
			//@{
		signals:

			void					modified( );
			//@}
			//-----------------------------------------------------------------
		};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qan_can_Style_h


