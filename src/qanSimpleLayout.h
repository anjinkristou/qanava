/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanSimpleLayout.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 January 06
//-----------------------------------------------------------------------------


#ifndef qanSimpleLayout_h
#define qanSimpleLayout_h


// Qanava headers
#include "./qanLayout.h"


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	//! Layout a graph as concentric circles of nodes.
	/*!
		\nosubgrouping
	*/
	class Concentric : public Layout
	{
		/*! \name Concentric Constructor/Destructor *///-----------------------
		//@{
	public:

		//! Concentric constructor.
		Concentric( double azimutDelta = 45., double circleInterval = 50. ) :
			Layout( ), _azimutDelta( azimutDelta ), _circleInterval( circleInterval ) { }

	private:

		double	_azimutDelta;

		double	_circleInterval;
		//@}
		//---------------------------------------------------------------------



		/*! \name Concentric Layout Management *///----------------------------
		//@{
	public:

		//! .
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 );
		//@}
		//---------------------------------------------------------------------
	};


	//! Layout nodes in a (logarithm) colimacon.
	/*!
		\nosubgrouping
	*/
	class Colimacon : public Layout
	{
		/*! \name Colimacon Constructor/Destructor *///------------------------
		//@{
	public:

		//! Colimacon constructor.
		Colimacon( double azimutDelta = 15., double circleInterval = 10. ) :
			Layout( ), _azimutDelta( azimutDelta ), _circleInterval( circleInterval ) { }

	private:

		double	_azimutDelta;

		double	_circleInterval;
		//@}
		//---------------------------------------------------------------------



		/*! \name Colimacon Layout Management *///-----------------------------
		//@{
	public:

		//! .
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 );
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qanSimpleLayout_h

