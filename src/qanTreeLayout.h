/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	qanTreeLayout.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 January 06
//-----------------------------------------------------------------------------


#ifndef qanTreeLayout_h
#define qanTreeLayout_h


// Qanava headers
#include "./qanLayout.h"


//-----------------------------------------------------------------------------
namespace qan { // ::qan


	//! Layout an undirected graph as a directed top-down tree.
	/*!
		\nosubgrouping
	*/
	class HierarchyTree : public Layout
	{
		/*! \name HierarchyTree Constructor/Destructor *///---------------------
		//@{
	public:


		//! HierarchyTree constructor with orientation initialization.
		/*! \param spacing spacing between node on x and y (ex: 120, 70 with Qt::Vertical).	*/
		HierarchyTree( QPointF spacing, QPointF origin = QPointF( 0., 0. ), Qt::Orientation orientation = Qt::Vertical ) :
			Layout( ), _spacing( spacing ), _origin( origin ), _orientation( orientation )
		{
			if ( orientation == Qt::Horizontal )	// Layout is computed for vertical, swap parameters when laying out horizontaly
			{
				_spacing.rx( ) = spacing.y( );
				_spacing.ry( ) = spacing.x( );
				_origin.rx( ) = origin.y( );
				_origin.ry( ) = origin.x( );
			}
		}
		//@}
		//---------------------------------------------------------------------



		/*! \name Hierarchy Layout Generation Management *///------------------
		//@{
	public:

		//! Layout a graph as a hierarchy tree.
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 );

	protected:

		//! Layout a node hierarchy as a hierarchy tree.
		void			layout( Node& node, QRectF& bbox, int depth, QProgressDialog* progress = 0, int step = 0 );

		QPointF			_origin;

		//! Spacing on x and y between tree nodes.
		QPointF			_spacing;

		//! Generated tree orientation.
		Qt::Orientation	_orientation;

		//! Container for already laid out nodes.
		Node::Set		_marked;
		//@}
		//---------------------------------------------------------------------
	};



	//!	Layout a tree using a greedy spatial optimisation based on the binary tree layout algorithm from Chan (1999).
	/*!
		While this layout algorithm is stricly upward and preserve node order, two node at the
		same depth might not be drawn at the same level (y). This algorithm might not be suitable for
		drawing hierarchy (use the HierarchyTree algorithm instead).

		\nosubgrouping
	*/
	class ChanTree : public Layout
	{
		/*! \name HierarchyTree Constructor/Destructor *///---------------------
		//@{
	public:

		//! .
		/*! */
		ChanTree( );
		//@}
		//---------------------------------------------------------------------



		/*! \name Hierarchy Layout Generation Management *///------------------
		//@{
	public:

		//! Layout a graph as a hierarchy tree.
		virtual void	layout( Graph& graph, QGraphicsScene* scene,
								QRectF r, QProgressDialog* progress = 0, int step = -1 );

	protected:

		void			layoutSubTree( Node& node, Node::Set& leafTrees, QMap< Node*, QRectF >& bboxes );

		void			applyLeftRule( Node& root, Node::List& left, Node::List& right, QMap< Node*, QRectF >& bboxes );

		void			applyRightRule( Node& root, Node::List& left, Node::List& right, QMap< Node*, QRectF >& bboxes );

		//! Perform a simple layout for a leaf subtree (node being the root of a tree of depth 2).
		/*!
			See "Drawing Graphs - Methods end Models" p50 for details.

			\param	node	root of a tree of depth 2.
		 */
		void			trivialLayout( Node& node, QMap< Node*, QRectF >& bboxes );

		//! Add all leaf tree root node to a given set.
		/*!
			\param	leafTrees	All result leaft tree root nodes will be added in this (not necessarilly empty) set.
			\return	return		Return value is used internally for recursion control and should be ignored.
		*/
		bool			findLeafTrees( Node& node, Node::Set& leafTrees );

		//! Transform all node coordinate from their local coordinate system the a global tree CS.
		/*!
			A VectorF( 2 ) with tree origin coordinates must be specified for the root node.

			\param	node	root node for the tree that must be translated.
		*/
		void			transformPositions( QGraphicsScene* scene, Node& node, VectorF current, QMap< Node*, QRectF >& bboxes, Node::Set& leafTrees );

		//! Compute a bounding box enclosing all bounding boxes for a given set of nodes.
		QRectF			computeHorizontalBbox( Node::List& nodes, QMap< Node*, QRectF >& bboxes );

		//! Recursively compute a bounding boxe for a given node.
		/*!
			Bounding box will be specified in the node local coordinate system, to obtain a bounding
			box valid in the super node, it must be translated by its node position.
		*/
		QRectF			computeBBox( Node& node, QMap< Node*, QRectF >& bboxes );

	private:

		QRectF			computeBBox( Node& node, VectorF current, QMap< Node*, QRectF >& bboxes );
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // qanTreeLayout_h

