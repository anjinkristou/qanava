/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canGraphModel.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2007 December 27
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanGraphModel.h"


//-----------------------------------------------------------------------------
namespace qan { // ::qan


/* Scene Interface Management *///---------------------------------------------
GraphModel::GraphModel( )
{

}

void	GraphModel::init( Node::List& rootNodes )
{
	// Generate persistent model indexes for all graph nodes
	Node::List::iterator nodeIter = rootNodes.begin( );
	for ( int row = 0; nodeIter != rootNodes.end( ); row++, nodeIter++ )
		visit( **nodeIter );
}

void	GraphModel::edgeInserted( qan::Edge& edge )
{
	// Remove any dst instance from the model root since it is now node with >1 indegree
	QList< QStandardItem* > dstItems = _nodeItemMap.values( &edge.getDst( ) );
	foreach ( QStandardItem* dstItem, dstItems )
		removeItemHierarchy( dstItem );

	// Create a hierarchy for the dst item with all (in edges) items src as parent
	Edge::List& inEdgesList = edge.getDst( ).getInEdges( );
	for ( Edge::List::iterator inEdgeIter = inEdgesList.begin( ); inEdgeIter != inEdgesList.end( ); inEdgeIter++ )
	{
		QList< QStandardItem* > srcItems = _nodeItemMap.values( &( *inEdgeIter )->getSrc( ) );
		foreach ( QStandardItem* srcItem, srcItems )
			addItemHierarchy( edge.getDst( ), srcItem );
	}
}

void	GraphModel::edgeRemoved( qan::Edge& edge )
{

}

void	GraphModel::nodeInserted( qan::Node& node )
{
	createNode( node );
}

void	GraphModel::nodeRemoved( qan::Node& node )
{
	QList< QStandardItem* > items = _nodeItemMap.values( &node );
	foreach ( QStandardItem* item, items )
	{
		QStandardItem* parent = item->parent( );
		if ( parent != 0 )
			parent->removeRow( item->row( ) );
		else
			invisibleRootItem( )->removeRow( item->row( ) );
	}
}

void	GraphModel::nodeChanged( qan::Node& node )
{

}

qan::Node*	GraphModel::getModelIndexNode( QModelIndex index )
{
	const QStandardItem* item = itemFromIndex( index );
	if ( item != 0 )
	{
		void* node = qvariant_cast< void* >( item->data( ) );
		return reinterpret_cast< qan::Node* >( node );
	}
	return 0;
}

void	GraphModel::visit( qan::Node& node )
{
	if ( _nodeItemMap.find( &node ) != _nodeItemMap.end( ) )
		return;
	createNode( node );

	// Visit sub nodes
	Edge::List& edgeList = node.getOutEdges( );
	for ( Edge::List::iterator edgeIter = edgeList.begin( ); edgeIter != edgeList.end( ); edgeIter++ )
	{
		Edge* edge = *edgeIter;
		visit( edge->getDst( ) );
	}
}

void	GraphModel::createNode( qan::Node& node )
{
	if ( node.getInDegree( ) == 0 )
	{
		QStandardItem* parent = invisibleRootItem( );
		addItem( node, parent );
	}
	else
	{
		Edge::List& inEdgesList = node.getInEdges( );
		for ( Edge::List::iterator edgeIter = inEdgesList.begin( ); edgeIter != inEdgesList.end( ); edgeIter++ )
		{
			Edge* edge = *edgeIter;
			QStandardItem* parent = _nodeItemMap.value( &edge->getSrc( ) );
			if ( parent != 0 )
				addItem( node, parent );
		}
	}
}

QStandardItem*	GraphModel::addItem( qan::Node& node, QStandardItem* parent )
{
	QStandardItem* item = new QStandardItem( node.getLabel( ) );
	QVariant data = qVariantFromValue( ( void* )( &node ) );
	item->setData( data );
	parent->appendRow( item );
	_nodeItemMap.insert( &node, item );
	return item;
}

void	GraphModel::removeItem( qan::Node& node )
{

}

void	GraphModel::removeItemHierarchy( QStandardItem* item )
{
	for ( int r = 0; r < item->rowCount( ); r++ )
		removeItemHierarchy( item->child( r ) );

	// Remove item mapping
	void* node = qvariant_cast< void* >( item->data( ) );
	if ( node != 0 )
		_nodeItemMap.remove( reinterpret_cast< qan::Node* >( node ) );

	// Remove item
	if ( item->parent( ) != 0 )
		item->parent( )->removeRow( item->row( ) );
	else
		invisibleRootItem( )->removeRow( item->row( ) );
}

void	GraphModel::addItemHierarchy( qan::Node& node, QStandardItem* parent, qan::Node::Set* marked )
{
	if ( marked != 0 && marked->find( &node ) != marked->end( ) )
		return;

	if ( parent != 0 )
	{
		parent = addItem( node, parent );
		if ( marked != 0 )
			marked->insert( &node );
	}

	Edge::List& outEdgesList = node.getOutEdges( );
	for ( Edge::List::iterator edgeIter = outEdgesList.begin( ); edgeIter != outEdgesList.end( ); edgeIter++ )
	{
		qan::Node::Set markedNodes;
		if ( marked == 0 )
			marked = &markedNodes;
		addItemHierarchy( ( *edgeIter )->getDst( ), parent, marked );
	}
}
//-----------------------------------------------------------------------------


} // ::qan
//-----------------------------------------------------------------------------


