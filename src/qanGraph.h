/*
Qanava - GraphT drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laGraph.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 February 15
//-----------------------------------------------------------------------------


#ifndef laGraph_h
#define laGraph_h


// Qanava headers
#include "./qanEdge.h"
#include "./qanNode.h"
#include "./qanGraphScene.h"


// QT headers
#include <QMap>
#include <QList>
#include <QStandardItemModel>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

		//! Model a standard weighted directed graph using a node-list, edge-list representation.
		/*!
			\nosubgrouping
		*/
		template < class M, class O >
		class GraphT
		{
		public:

			/*! \name GraphT Constructor/Destructor *///------------------------
			//@{
		public:

			//! GraphT default constructor.
			GraphT( );

			//! GraphT default destructor. All registered nodes and edges are invalid after graph destruction.
			~GraphT( );

			M&			getM( ) { return _m; }
			const M&	getM( ) const { return _m; }

			O&			getO( ) { return _o; }
			const O&	getO( ) const { return _o; }

		private:

			//! GraphT empty private copy constructor.
			GraphT( const GraphT& graph );

			M	_m;
			O	_o;
			//@}
			//-----------------------------------------------------------------



			/*! \name Edge/Node Management *///--------------------------------
			//@{
		public:

			//! Clear the graph from all its edges and nodes.
			void			clear( );

			//! Set a node object unique key.
			void			setNodeObject( Node& node, void* object );

			void			modifyNode( Node& node, const QString& name, int type = -1, void* object = 0 );

			void			modifyEdge( Edge& edge, Node& a, Node& b, float weight = 1.f );

			typedef			QList< GraphT< M, O >* >	List;
			//@}
			//-----------------------------------------------------------------


			/*! \name Edge/Node Management *///--------------------------------
			//@{
		public:

			void			addNode( Node* node, void* object = 0 );

			Node*			addNode( const QString& name, int type = -1, void* object = 0 );

			void			insertNode( Node* node, void* object = 0 );

			Node*			insertNode( const QString& name, int type = -1, void* object = 0 );

			void			removeNode( Node& node );


			void			addEdge( Edge* edge );

			Edge*			addEdge( Node& a, Node& b, float weight = 1.f );

			void			insertEdge( Edge* edge );

			Edge*			insertEdge( Node& a, Node& b, float weight = 1.f );

			//! Insert a new leaf node in this graph using an existing node as the super node, and return an edge from the super node to node.
			/*!
				\param	node		Node that must be inserted in the graph (graph will take ownership of the node).
				\param	superNode	Already existing node that will be used as a super node, and the source of the returned edge.
			 */
			Edge*			insertNode( Node* node, Node& superNode );

			void			removeEdge( Edge& edge );

			bool			hasEdge( Node& a, Node& b ) const;

			//! Take care of updating models when topology has been initialized using create* methods.
			/*! This method is usualy usefull before connecting a graph to a view when it has been initialized
				whitout using the insert methods who are automatically taking care of updating models.	*/
			void			updateModels( );

		private:

			//! List of nodes currently registered in this graph.
			Node::List		_nodes;

			//! Set of nodes currently registered in this graph (used for fast node search).
			Node::Set		_nodesSet;

 			//! Map an object to its associed node.
			typedef QMap< void*, Node* >	ObjectNodeMap;

 			//! .
			typedef QMap< Node*, void* >	NodeObjectMap;

			//! Map an object to its associed graph node.
			ObjectNodeMap	_objectNodeMap;

			//! .
			NodeObjectMap	_nodeObjectMap;

			//! List of edges currently connecting the registered nodes in this graph.
			Edge::List		_edges;
			//@}
			//-----------------------------------------------------------------




			/*! \name GraphT Consultation Management *///-----------------------
			//@{
		public:

			//! Get graph node count.
			unsigned int	getNodeCount( ) const { return _nodes.size( ); }

			//! Get graph's nodes list (list must be used read-only).
			Node::List&		getNodes( ) { return _nodes; }

			//! Get graph's edges list (list must be used read-only).
			Edge::List&		getEdges( ) { return _edges; }

			//! Collect a set of unique node registered in this graph.
			void			collectNodes( Node::Set& nodes ) const;
			//@}
			//-----------------------------------------------------------------



			/*! \name Search Management *///-----------------------------------
			//@{
		public:

			//! Find a node of a given label in the graph.
			Node*			findNode( const QString& label );

			//! Find a node given its associed key memory object (if such key has been specified during node adjunction).
			Node*			findNode( void* object );

			//! Find the nth registered node in this graph (For internal use only).
			/*! This method should only be used in repositories just after node loading when their initial order has not been altered. */
			Node*			findNode( int node );

			//! Get the index where a node is currently registered in this graph.
			/*! This method should only be used in repositories just after node loading when their initial order has not been altered. */
			int				findNode( const Node& node ) const;

			//! Find a node object for a specific node (if such an object has been specified during node adjunction).
			void*			findObject( Node* node );

			//! Search for a specific node in this graph and return true if the node is found.
			bool			hasNode( Node* node ) const;
			//@}
			//-----------------------------------------------------------------



			/*! \name Root Node Management *///--------------------------------
			//@{
		public:

			//! Add a root to this graph (ie a node that doesn't have super nodes.)
			/*! \param node must be already registered in the graph. 		*/
			void			addRootNode( Node& node ) { _rootNodes << &node; _rootNodesSet << &node; }

			//! Remove a node from the root node list
			void			removeRootNode( Node& node ) { _rootNodes.removeAll( &node ); _rootNodesSet.remove( &node ); }

			//! Get the currently registered root node for this graph.
			Node::List&		getRootNodes( ) { return _rootNodes; }

			//! Get the currently registered root node for this graph.
			Node::Set&		getRootNodesSet( ) { return _rootNodesSet; }

			//! Generate the root set by automatically looking for nodes with no super nodes (O(n), n numbers of nodes).
			void			generateRootNodes( );

			//! Test if a given node is a graph root node.
			bool			isRootNode( const Node& node ) const;

		private:

			//! Ordered root nodes for this graph's subgraphs.
			Node::List		_rootNodes;

			//! Root nodes of graph's subgraphs (used only to test root node existence).
			Node::Set		_rootNodesSet;
			//@}
			//-----------------------------------------------------------------


			/*! \name Node Attributes Management *///--------------------------
			//@{
		public:

			template < typename T >
				void		addAttribute( const QString& name, T t )
			{
				_attributesNames.push_back( name );
				for ( Node::List::iterator nodeIter = getNodes( ).begin( ); nodeIter != getNodes( ).end( ); nodeIter++ )
					( *nodeIter )->addAttribute< T >( t );
			}

			QString			getAttributeName( int role )
			{
				return ( role < _attributesNames.size( ) ? _attributesNames[ role ] : QString( "" ) );
			}

			template < typename T >
				void		setAttribute( Node* node, const QString& name, T& value )
			{
				int role = hasAttribute( name );
				if ( role >= 0)
					setAttribute< T >( node, role, value );
			}

			template < typename T >
				void		setAttribute( Node* node, unsigned int role, T& value )
			{
				node->setAttribute< T >( role, value );
			}

			int				hasAttribute( const QString& name ) const
			{
				int r = 0;
				Strings::const_iterator nameIter = _attributesNames.begin( );
				for ( ; nameIter != _attributesNames.end( ); nameIter++, r++ )
					if ( *nameIter == name )
						return r;
				return -1;
			}

			unsigned int	getAttributesCount( ) const { return _attributesNames.size( ); }

		protected:

			void			initNodeAttributes( Node& node )
			{
				node.initAttributes( _attributesNames.size( ) );
			}

			typedef	QList< QString >	Strings;

			Strings			_attributesNames;
			//@}
			//-----------------------------------------------------------------
		};

		typedef	GraphT< GraphScene, GraphModel > Graph;
} // ::qan

#include "./qanGraph.hpp"
//-----------------------------------------------------------------------------


#endif // laGraph_h

