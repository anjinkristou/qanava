/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	uiNodesItemModel.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2006 August 30
//-----------------------------------------------------------------------------


// Qanava headers
#include "./uiNodesItemModel.h"


// QT headers
#include <QFont>


namespace qan { // ::qan
namespace ui  { // ::qan::ui


//-----------------------------------------------------------------------------
NodesItemModel::NodesItemModel( Graph& graph, QObject *parent ) :
	_graph( graph )
{
	connect( &graph.getO( ), SIGNAL( rowAboutToBeInserted(const QModelIndex&,int,int) ), this, SLOT( nodeAboutToBeInserted(const QModelIndex&,int,int) ) );
	connect( &graph.getO( ), SIGNAL( rowAboutToBeRemoved(const QModelIndex&,int,int) ), this, SLOT( nodeAboutToBeRemoved(const QModelIndex& parent,int,int) ) );
	connect( &graph.getO( ), SIGNAL( rowInserted(const QModelIndex&,int,int) ), this, SLOT( nodeInserted(const QModelIndex&,int,int) ) );
	connect( &graph.getO( ), SIGNAL( rowRemoved(const QModelIndex&,int,int) ), this, SLOT( nodeRemoved(const QModelIndex&,int,int) ) );
}


QVariant NodesItemModel::data( const QModelIndex &index, int role ) const
{
	if ( !index.isValid( ) )
		return QVariant( "index invalid" );

	QVariant d;

	if ( ( role == Qt::FontRole ) && ( index.column( ) == 0 ) )
	{
		QFont font;
		font.setWeight( QFont::Bold );
		d = font;
	}

	if ( role == Qt::DisplayRole )
	{
		Node* node = _graph.findNode( index.row( ) );
		if ( node != 0 )
		{
			if ( index.column( ) == 0 )
				d =	QString( node->getLabel( ) );
			if ( index.column( ) > 0 )
			{
				int attributeRole = index.column( ) - 1 + Node::StdAttributeCount;
				if ( attributeRole < ( int )_graph.getAttributesCount( ) )
				{
					QString* attribute = node->getAttribute< QString >( attributeRole );
					d = ( attribute != 0 ? QVariant( *attribute ) : QVariant( "" ) );
				}
			}
		}
	}
	return d;
}

bool NodesItemModel::hasChildren( const QModelIndex& parent ) const
{
	return false;
}

Qt::ItemFlags NodesItemModel::flags( const QModelIndex& index ) const
{
	return ( index.isValid( ) ? Qt::ItemIsSelectable | Qt::ItemIsEnabled : Qt::ItemIsEnabled );
}

QVariant NodesItemModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	QVariant d;
	if ( role == Qt::DisplayRole )
	{
		int attributeRole = section - 1 + Node::StdAttributeCount;
		if ( section == 0 )
			return QString( "Label" );
		else if ( attributeRole >= 0 && attributeRole < ( int )_graph.getAttributesCount( ) )
			d = _graph.getAttributeName( attributeRole );
	}
	return d;
}

QModelIndex NodesItemModel::index(int row, int column, const QModelIndex& parent ) const
{
	return createIndex( row, column, ( row * 10000 ) + column );
}

QModelIndex NodesItemModel::parent( const QModelIndex &index ) const
{
	return QModelIndex( );
}

int NodesItemModel::rowCount( const QModelIndex& parent ) const
{
	return _graph.getNodeCount( );
}

int	NodesItemModel::columnCount( const QModelIndex& parent ) const
{
	return ( 1 + _graph.getAttributesCount( ) - Node::StdAttributeCount );
}

void	NodesItemModel::nodeAboutToBeInserted( const QModelIndex& parent, int start, int end )
{
	QModelIndex nodeItem = _graph.getO( ).index( start, 0, parent );
	if ( nodeItem.isValid( ) )
	{
		Node* node = static_cast< Node* >( nodeItem.internalPointer( ) );
		int id = _graph.findNode( *node );
		if ( id != -1 )
			beginInsertRows( QModelIndex( ), id, id + 1 );
	}
}

void	NodesItemModel::nodeAboutToBeRemoved( const QModelIndex& parent, int start, int end )
{
/*	int id = _graph.findNode( node );
	if ( id != -1 )
		beginRemoveRows( QModelIndex( ), id, id + 1 );*/
}

void	NodesItemModel::nodeInserted( const QModelIndex& parent, int start, int end )
{
	QModelIndex nodeItem = _graph.getO( ).index( start, 0, parent );
	if ( nodeItem.isValid( ) )
	{
		Node* node = static_cast< Node* >( nodeItem.internalPointer( ) );
		int id = _graph.findNode( *node );
		if ( id != -1 )
			emit rowsInserted( QModelIndex( ), id, id + 1 );
		emit layoutChanged( );
	}
}

void	NodesItemModel::nodeRemoved( const QModelIndex& parent, int start, int end )
{
	/*int id = _graph.findNode( node );
	if ( id != -1 )
		endRemoveRows( );
	emit layoutChanged( );*/
}
//-----------------------------------------------------------------------------


} // ::qan::ui
} // ::qan

