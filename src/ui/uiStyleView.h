/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	uiStyleView.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 December 23
//-----------------------------------------------------------------------------


#ifndef uiStyleEditor_h
#define uiStyleEditor_h


// Qanava headers
#include "../qanStyle.h"


// QT headers
#include <QWidget>
#include <QMainWindow>
#include <QAbstractItemModel>
#include <QTableView>
#include <QItemDelegate>
#include <QLabel>
#include <QLineEdit>
#include <QComboBox>


//-----------------------------------------------------------------------------
namespace qan // ::qan
{
		class ColorEditWidget : public QWidget
		{
			Q_OBJECT

		public:

			ColorEditWidget( QWidget* parent, QColor color );

			QColor	getColor( ) { return _color; }

			void	setColor( QColor color );

		protected:

			QLabel*	_label;

			QColor	_color;

		private slots:

			void	selectColor( );
		};

		class StringEditWidget : public QWidget
		{
			Q_OBJECT

		public:

			StringEditWidget( QWidget* parent, QString s );

			QString	getString( ) { return _string; }

			void	setString( QString s );

		protected:

			QLineEdit*	_edit;

			QString		_string;

		private slots:

			void	selectString( );

			void	editingFinished( );
		};

		class StyleDelegate : public QItemDelegate
		{
		public:

			StyleDelegate( QAbstractItemModel& model /*QAbstractItemView& tableView*/ );

			virtual	QWidget*	createEditor ( QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const;

			virtual	void		paint( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;

			virtual void		setEditorData( QWidget* editor, const QModelIndex& index ) const;

			virtual	void		setModelData( QWidget* editor, QAbstractItemModel* model, const QModelIndex& index ) const;

			virtual	QSize		sizeHint ( const QStyleOptionViewItem& option, const QModelIndex& index ) const;

		protected:

			QAbstractItemModel&		_model;
		};



		//!
		/*!
			\nosubgrouping
		*/
		class StyleEditor : public QMainWindow
		{
			Q_OBJECT

		public:

			StyleEditor( QWidget* parent = 0, Style* style = 0 );

			virtual ~StyleEditor( ) { }

		private:

			StyleEditor( const StyleEditor& );

			StyleEditor& operator=( const StyleEditor& );

		public:

			void	setStyle( Style* style );

			virtual	QSize	sizeHint( ) const { return QSize( 150, 200 ); }

		protected slots:

			void	addAttribute( );

			void	removeAttribute( );

		protected:

			Style*		_style;

			QTableView*	_tableView;

			QComboBox*	_cbType;
		};
} // ::qan
//-----------------------------------------------------------------------------


#endif // uiStyleEditor_h

