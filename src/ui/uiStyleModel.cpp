/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Mysire software.
//
// \file	uiStyleItemModel.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2006 January 04
//-----------------------------------------------------------------------------


// Qanava headers
#include "./uiStyleModel.h"


namespace qan { // ::qan
namespace ui  { // ::qan::ui


//-----------------------------------------------------------------------------
	StyleItemModel::StyleItemModel( Style& style, QObject *parent ) :
	QAbstractItemModel( parent ),
	_style( style )
{

}
//-----------------------------------------------------------------------------



/* Qt Model Interface *///-----------------------------------------------------
QVariant	StyleItemModel::data( const QModelIndex &index, int role ) const
{
	if ( !index.isValid( ) )
		return QVariant( "index invalid" );

	QVariant d;

	if ( ( role == Qt::FontRole ) && ( index.column( ) == 0 ) )
	{
		QFont font;
		font.setWeight( QFont::Bold );
		d = font;
	}

	if ( role == Qt::DisplayRole )
	{
		if ( index.column( ) == 0 )
		{
			d =	QString( "ATTR NAME" );
		}
		else if ( index.column( ) == 1 )
		{
			d = QString( "FIXME: StyleItemModel::data()!" );
		}
	}

	return d;
}

bool	StyleItemModel::hasChildren( const QModelIndex & parent ) const
{
	return false; // No hierarchy for articles
}

Qt::ItemFlags	StyleItemModel::flags( const QModelIndex &index ) const
{
	if ( !index.isValid( ) )
		return Qt::ItemIsEnabled;

	if ( index.column( ) == 0 )
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
	else if ( index.column( ) == 1 )
		return Qt::ItemIsEnabled | Qt::ItemIsEditable;

	return Qt::ItemIsEnabled;
}

QVariant	StyleItemModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
	if ( orientation == Qt::Horizontal && role == Qt::DisplayRole )
	{
		if ( section == 0 )
			return QString( "Property" );
		else if ( section == 1 )
			return QString( "Value" );
	}
	return QVariant( );
}

QModelIndex	StyleItemModel::index( int row, int column, const QModelIndex &parent ) const
{
	if ( !parent.isValid( ) )
	{
		const void* internalPointer = 0;
		int internalId = 0;

		//Style::Attribute* attribute = _style.getAttributeManager( ).get( row );
		//if ( attribute != 0 )
		//{
			if ( column == 0 )
				internalId = 100 + row;
			else if ( column == 1 )
				internalId = 200 + row;
				// FIXME internalPointer = 200 + row; // FIXME attribute;
		//}

		if ( internalPointer != 0 )
			return createIndex( row, column, const_cast< void* >( internalPointer ) );
		else if ( internalId != 0 )
			return createIndex( row, column, internalId );
	}
	return QModelIndex( );
}

QModelIndex	StyleItemModel::parent( const QModelIndex &index ) const
{
	return QModelIndex( ); // No hierarchy for articles
}

int	StyleItemModel::rowCount( const QModelIndex& parent ) const
{
	return _style.size( );
}

int	StyleItemModel::columnCount( const QModelIndex& parent ) const
{
	return 2;
}
//-----------------------------------------------------------------------------


} // ::qan::ui
} // ::qan
