/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laEdge.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 February 15
//-----------------------------------------------------------------------------


// Qanava headers
#include "./qanEdge.h"
#include "./qanNode.h"


namespace qan { // ::qan

void	Edge::set( Node* src, Node* dst )
{
	if ( src != 0 && dst != 0 )
	{
		_src = src; _dst = dst;
		src->addOutEdge( *this );
		dst->addInEdge( *this );
	}
}

} // ::qan
