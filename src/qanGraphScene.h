/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canGraphScene.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 22
//-----------------------------------------------------------------------------


#ifndef canGraphScene_h
#define canGraphScene_h


// Qanava headers
#include "./qanNodeItem.h"
#include "./qanEdgeItem.h"
#include "./qanGraphModel.h"


// QT headers
#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QMap>
#include <QStandardItemModel>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

	class Grid;


	//! Display a graph provided as a QT Interview based model.
	/*!
		\nosubgrouping
	*/
	class GraphScene : public GraphListener, public QGraphicsScene
	{
		/*! \name GraphScene Object Management *///----------------------------
		//@{
	public:

		//! GraphScene constructor with optionnal settings for graph widget initialization.
		GraphScene( QWidget* parent = 0, QColor backColor = QColor( 170, 171, 205 ), QSize size = QSize( 300, 150 ) );

		//! GraphScene virtual destructor.
		virtual ~GraphScene( );
		//@}
		//---------------------------------------------------------------------



		/*! \name Scene Management *///----------------------------------------
		//@{
	public:

		//! Clear the scene from all its graphics elements (styles are not destroyed).
		void			clear( );

		//! Get bounding box for a group of nodes.
		static VectorF	getBoundingBox( const Node::List& nodes );

		//! .
		void			updatePositions( Node* except = 0 );
		//@}
		//---------------------------------------------------------------------



		/*! \name Style Management *///----------------------------------------
		//@{
	public:

		//! Get the graph view style manager.
		Style::Manager&	getStyleManager( ) { return _styleManager; }

		//! Set a node or edge (or other graphic object) style.
		void			applyStyle( void* item, Style* style );

		void			updateStyle( void* item, Style* style );

	protected:

		//! Graph view style manager.
		Style::Manager	_styleManager;
		//@}
		//---------------------------------------------------------------------



		/*! \name Scene Topology Management *///-------------------------------
		//@{
	public:

		void	init( Node::List& rootNodes );

	public:

		void	edgeInserted( qan::Edge& edge );

		void	edgeRemoved( qan::Edge& edge );

		void	nodeInserted( qan::Node& node );

		void	nodeRemoved( qan::Node& node );

		void	nodeChanged( qan::Node& node );

		void	visit( qan::Node& node );

	private:

		void	insertNode( qan::Node& node );

		void	insertEdge( qan::Edge& edge );
		//@}
		//---------------------------------------------------------------------



		/*! \name Node and Graphic Item Mapping Management *///----------------
		//@{
	public:

		//! Get a graph node associed to a given graphic item.
		Node*			getGraphicItemNode( const QGraphicsItem* item );

		//! Get a graphic item associed to a given graph node.
		QGraphicsItem*	getNodeGraphicItem( const Node* node );

		//! Get the node that is currently under the mouse pointer.
		Node*			getCurrentNode( QPoint& p );

	signals:

		void			nodeDoubleClicked( qan::Node* node );

	protected:

		typedef QMap< const Edge*, EdgeItem* >			EdgeGraphicItemMap;

		typedef QMap< const Node*, AbstractNodeItem* >	NodeGraphicItemMap;

		EdgeGraphicItemMap			_edgeGraphicItemMap;

		NodeGraphicItemMap			_nodeGraphicItemMap;
		//@}
		//---------------------------------------------------------------------



		/*! \name Graphic Item Node Factory Management *///--------------------
		//@{
	public:

		//! Register a graphic node factory for a certain type of nodes (from graph topology to graph graphic visualization).
		void	registerGraphicNodeFactory( AbstractNodeItem::AbstractFactory* factory );

		//! Create a graphic node from a topological node using the currently registered graphic node factories.
		AbstractNodeItem*	createNodeItem( Node& node, QGraphicsItem* parent, QPoint origin, const QString& label );

	private:

		AbstractNodeItem::AbstractFactory::List	_factories;
		//@}
		//---------------------------------------------------------------------
	};
} // ::qan
//-----------------------------------------------------------------------------


#endif // canGraphScene_h

