/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	laEdge.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 February 15
//-----------------------------------------------------------------------------


#ifndef laEdge_h
#define laEdge_h


// Qanava headers
#include "./qanConfig.h"


// Standard headers
#include <string>
#include <list>
#include <set>
#include <cassert>


// QT headers
#include <QString>
#include <QList>
#include <QSet>


//-----------------------------------------------------------------------------
namespace qan { // ::qan

		class Node;


		//! Allow an inheritor classe to manage a set of attributes.
		/*!
			initAttributes() method must be call in the direct inheritor ctor if only the
			default constructor has been used.
		 */
		class AttrFunc
		{
			/*! \name Node Attributes Management *///--------------------------
			//@{
		public:

			AttrFunc( ) { initAttributes( 0 ); }

			AttrFunc( unsigned int attributeCount ) { initAttributes( attributeCount ); }

			template < typename T >
			void				addAttribute( T t )
			{
				_attributes.push_back( new T( t ) );
			}

			template < typename T >
			T*					getAttribute( int role )
			{
				assert( role < _attributes.size( ) );
				return ( _attributes[ role ] != 0 ? static_cast< T* >( _attributes[ role ] ) : 0 );
			}

			template < typename T >
			const T*					getAttribute( int role ) const
			{
				assert( role < _attributes.size( ) );
				return ( _attributes[ role ] != 0 ? static_cast< const T* >( _attributes[ role ] ) : 0 );
			}

			template < typename T >
			void				setAttribute( int role, const T& t )
			{
				assert( role < _attributes.size( ) );
				if ( _attributes[ role ] == 0 )
					_attributes[ role ] = new T( t );
				else
					*static_cast< T* >( _attributes[ role ] ) = t;
			}

			void				initAttributes( unsigned int attributeCount )
			{
				if ( _attributes.size( ) < ( int )attributeCount )   // Add attributes to fit the requested attribute count
				{
					for ( unsigned int attribute = _attributes.size( ); attribute < attributeCount; attribute++ )
						_attributes.push_back( 0 );
				}
			}

		private:

			QList< void* >	_attributes;
			//@}
			//-----------------------------------------------------------------
		};



		//! Model a weighted directed edge between two nodes.
		/*!
			\nosubgrouping
		*/
		class Edge : public AttrFunc
		{
			//! Attribute role.
			enum Role
			{
				WEIGHT			= 0,
				LABEL			= 1,
				USER			= 2
			};

			/*! \name Generator Constructor/Destructor *///--------------------
			//@{
		public:

			//! Node constructor with source and destination initialization.
			Edge( Node& src, Node& dst, float weight = 1.f ) : AttrFunc( 2 ),
				_src( &src ),
				_dst( &dst )
			{
				setAttribute( WEIGHT, weight );
				setLabel( QString( "" ) );
			}

			//! Node constructor with source and destination initialization.
			Edge( Node* src, Node* dst, float weight = 1.f ) : AttrFunc( 2 ),
				_src( src ),
				_dst( dst )
			{
				setAttribute( WEIGHT, weight );
				setLabel( QString( "" ) );
			}

			//! Typedef for a QT list of Edge.
			typedef	QList< Edge* >	List;

			//! Typedef for a QT set of Edge.
			typedef	QSet< Edge* >	Set;
			//@}
			//-----------------------------------------------------------------



			/*! \name Source/Destination Management *///-----------------------
			//@{
		public:

			//! Get edge source.
			bool		hasSrc( ) const { return ( _src != 0 ); }

			//! Get edge destination.
			bool		hasDst( ) const { return ( _dst != 0 ); }

			//! Get edge source.
			Node&		getSrc( ) { return *_src; }

			//! Get edge destination.
			Node&		getDst( ) { return *_dst; }

			//! Get const reference on the edge source.
			const Node&	getSrc( ) const { return *_src; }

			//! Get const reference on the edge destination.
			const Node&	getDst( ) const { return *_dst; }

			//! Get edge's weight.
			float		getWeight( ) const { return *getAttribute< float >( Edge::WEIGHT ); }

			//! Set edge's weight.
			void		setWeight( float weight ) { setAttribute< float >( Edge::WEIGHT, weight ); }

			//! Set edge source and destination (use this method carefully it is normally reserved for serialization implementation).
			void		set( Node* src, Node* dst );

			//! Get this edge label.
			const QString&	getLabel( ) const { return *getAttribute< QString >( Edge::LABEL ); }

			//! Set this edge label.
			void			setLabel( const QString& label ) { setAttribute< QString >( Edge::LABEL, label ); }

		private:

			//! Edge source.
			Node*	_src;

			//! Edge destination.
			Node*	_dst;
			//@}
			//-----------------------------------------------------------------
		};
} // ::qan
//-----------------------------------------------------------------------------


#endif // laEdge_h
