/*
Qanava - Graph drawing library for QT
Copyright (C) 2006 Benoit AUTHEMAN

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

//-----------------------------------------------------------------------------
// This file is a part of the Qanava and LTM software.
//
// \file	utlConfig.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 September 21
//-----------------------------------------------------------------------------


#ifndef qan_utlConfig_h
#define qan_utlConfig_h


// Configure this file either for LTM or Qanava
#undef QAN_UTL_ROOT_NAMESPACE
#define QAN_UTL_ROOT_NAMESPACE qan      // Uncomment for Qanava
//#define QAN_UTL_ROOT_NAMESPACE ltm    // Uncomment for LTM

#include <math.h>

#ifndef QANAVA_UNIX

#pragma warning( disable:4786 )  // VC6: Too long template name in debug informations

#pragma warning( disable:4251 )  // VC7.1: BOOST dll interface warning

#pragma warning( disable:4275 )  // VC7.1: BOOST dll interface warning

#pragma warning( disable:4290 )  // VC7.1: Explicit C++ exception specification

#pragma warning( disable:4100 )  // VC7.1: Unreferenced formal parameter

#pragma warning( disable:4244 )  // VC7.1: BOOST date and time warning

#endif

#endif // utlConfig_h

