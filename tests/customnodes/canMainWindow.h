//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


#ifndef canMainWindow_h
#define canMainWindow_h


// Qanava headers
#include "./qanGraph.h"
#include "../../src/qanGraphView.h"
#include "../../src/qanNodeItem.h"
#include "../../src/qanStyle.h"

#include "ui_canMainWindow.h"


// QT headers
#include <QMainWindow>
#include <QComboBox>
#include <QGraphicsItem>
#include <QTimer>


//-----------------------------------------------------------------------------
	class MyNode : public qan::AbstractNodeItem, public QGraphicsItem
	{
		Q_OBJECT

	public:

		MyNode( qan::Node& node, qan::Style::Manager& styleManager, qan::Style& style,
				QGraphicsItem* parent, QGraphicsScene* scene,
				QPoint origin, const QString& label ) :
		AbstractNodeItem( node, styleManager, &style ),
		QGraphicsItem( parent, scene )
		{
			setFlag( QGraphicsItem::ItemIsMovable );
			setZValue( 1.5 );
		}

		virtual ~MyNode( ) { }

		virtual QGraphicsItem*		getGraphicsItem( ) { return static_cast< QGraphicsItem* >( this ); }

		QRectF		boundingRect( ) const
		{
			qreal adjust = 2;
			return QRectF(-10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
		}

		void		paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
		{
			painter->setPen(Qt::NoPen);
			painter->setBrush(Qt::darkGray);
			painter->drawEllipse(-7, -7, 20, 20);

			QRadialGradient gradient(-3, -3, 10);
			if ( option->state & QStyle::State_Sunken ) {
				gradient.setCenter(3, 3);
				gradient.setFocalPoint(3, 3);
				gradient.setColorAt(1, QColor(Qt::yellow).light(120));
				gradient.setColorAt(0, QColor(Qt::darkYellow).light(120));
			} else {
				gradient.setColorAt(0, Qt::yellow);
				gradient.setColorAt(1, Qt::darkYellow);
			}
			painter->setBrush(gradient);
			painter->setPen(QPen(Qt::black, 0));
			painter->drawEllipse(-10, -10, 20, 20);
		}

		QVariant	itemChange( GraphicsItemChange change, const QVariant& value );

	public slots:

		virtual void	updateNode( )
		{

		}

	public:

		struct	Factory : public qan::AbstractNodeItem::Factory< MyNode >
		{
			Factory( qan::Style::Manager& styleManager ) :
				qan::AbstractNodeItem::Factory< MyNode >( 2, styleManager, false ) { }  // Factory for node of type '2', no default
		};
	};


		//!
		/*!
			\nosubgrouping
		*/
		class MainWindow : public QMainWindow, public Ui::MainWindow
		{
			Q_OBJECT

		public:

			MainWindow( QWidget* parent = 0 );

			virtual ~MainWindow( ) { }

		private:

			MainWindow( const MainWindow& );

			MainWindow& operator=( const MainWindow& );

			QComboBox*		_cbGridType;

			qan::GraphView*	_graphView;

		protected slots:

			void	selectNode( qan::Node* node, QPoint p );

			void	layoutGraph( );

		protected:

			QTimer*					_timer;

			qan::UndirectedGraph*	_layout;
		};
//-----------------------------------------------------------------------------


#endif //

