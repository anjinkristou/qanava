//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


#ifndef canMainWindow_h
#define canMainWindow_h


// Qanava headers
#include "./qanGraph.h"
#include "../../src/qanGraphView.h"
#include "ui_canMainWindow.h"


// QT headers
#include <QMainWindow>


//-----------------------------------------------------------------------------
//!
/*!
	\nosubgrouping
*/
		class MainWindow : public QMainWindow, public Ui::MainWindow
		{
			Q_OBJECT

		public:

			MainWindow( QWidget* parent = 0 );

			virtual ~MainWindow( ) { }

		private:

			MainWindow( const MainWindow& );

			MainWindow& operator=( const MainWindow& );
		};
//-----------------------------------------------------------------------------


#endif //

