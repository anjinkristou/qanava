
LANGUAGE = C++
DEFINES = QANAVA
TARGET = test_large
CONFIG += debug \
          warn_on \
          qt
TEMPLATE = app
FORMS += canMainWindow.ui
HEADERS += canMainWindow.h
SOURCES += canApp.cpp canMainWindow.cpp
macx | unix{
  QMAKE_CXXFLAGS_WARN_ON += -Wno-unused-parameter
  UI_DIR = .ui
  MOC_DIR = .moc
  OBJECTS_DIR = .obj
  INCLUDEPATH += ../../src/
  DEFINES += QANAVA_UNIX
  LIBS += -L../../build -lqanava
}
win32{
  OBJECTS_DIR = ./Debug
  INCLUDEPATH += ../../src/
  LIBS += ../../build/qanava.lib
}
