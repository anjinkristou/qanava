//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanGrid.h"
#include "../../src/qanSimpleLayout.h"
#include "./canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QAction>
#include <QMessageBox>
#include <QTimer>


using namespace qan;


void	generateBranch( Graph& _graph, Node& superNode, int depth, QProgressDialog* progress, QApplication* app, bool root = false )
{
	if ( --depth < 1 )
		return;

	int subNodeCount = 5 + ( rand( ) % 5 );
	app->processEvents( );
	if ( root )
		progress->setMaximum( subNodeCount );

	for ( int n = 0; n < subNodeCount; n++ )
	{
		if ( root )
		{
			app->processEvents( );
			progress->setValue( n );
			if ( progress->wasCanceled( ) )
				break;
		}

		QString label;
		label = "Node ";
		label += QString::number( _graph.getNodeCount( ) );
		Node* node = _graph.addNode( label );
		_graph.addEdge( superNode, *node );
		generateBranch( _graph, *node, depth, progress, app );
	}
}

void	generateTree( Graph& _graph, QProgressDialog* progress, QApplication* app, int depth = 5 )
{
	Node* root = _graph.addNode( "Root" );
	if ( root != 0 )
		generateBranch( _graph, *root, depth, progress, app, true );
}

//-----------------------------------------------------------------------------
MainWindow::MainWindow( QApplication* application, QWidget* parent ) :
	QMainWindow( parent ),
	_application( application )
{
	setupUi( this );

    QHBoxLayout *hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );
	hbox->setSpacing( 2 );

	// Setup zoom and navigation toolbars
	QToolBar* zoomBar = addToolBar( "Zooming" );
	QSlider* sldZoom = new QSlider( Qt::Horizontal, zoomBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	sldZoom->setMinimumWidth( 190 );
	zoomBar->addWidget( sldZoom );

	QToolBar* navigationBar = addToolBar( "Navigation" );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	// Generate a simple _graph
	_graph = new Graph( );

	// Setup item views
	_graphView = new qan::GraphView( _frame, QColor( 170, 171, 205 ) );
	GridItem* grid = new GridCheckBoardItem( _graphView, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	hbox->addWidget( _graphView );

	// Add canvas pan and zoom action to the navigation tobar
	_cbGridType = new QComboBox( navigationBar );
	_cbGridType->addItem( QIcon(), "<< Grid Type >>" );
	_cbGridType->addItem( QIcon(), "Regular" );
	_cbGridType->addItem( QIcon(), "Check Board" );
	connect( _cbGridType, SIGNAL(activated(int)), this, SLOT(gridChanged(int)));
	navigationBar->addWidget( _cbGridType );

	connect( sldZoom, SIGNAL( valueChanged(int) ), _graphView, SLOT( setZoomPercent(int) ) );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	if ( _graphView->getAction( "zoom_in" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_in" ) );
	if ( _graphView->getAction( "zoom_out" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_out" ) );
	navigationBar->addSeparator( );
	if ( _graphView->getAction( "pan_controller" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "pan_controller" ) );
	if ( _graphView->getAction( "zoom_window_controller" ) != 0 )
		navigationBar->addAction( _graphView->getAction( "zoom_window_controller" ) );


	QTimer::singleShot( 0, this, SLOT( loadGraph( ) ) );
}

void	MainWindow::loadGraph( )
{
	QProgressDialog* progress = new QProgressDialog( "Generating graph...", "Cancel", 0, 100, this );
	progress->setWindowModality( Qt::WindowModal );
	progress->show( );
	generateTree( *_graph, progress, _application, 5 );
	progress->close( );

	_graphView->setGraph( *_graph );

	qan::VectorF origin( 2 ); origin( 0 ) = 10.f; origin( 1 ) = 10.f;
	qan::VectorF spacing( 2 ); spacing( 0 ) = 120.f; spacing( 1 ) = 55.f;
	_graphView->setGraphLayout( new qan::Concentric( 20., 60. ) );
	_graphView->layoutGraph( new QProgressDialog( "Laying out...", "Cancel", 1, 100, this ) );
}
//-----------------------------------------------------------------------------


