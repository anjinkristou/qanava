//-----------------------------------------------------------------------------
// This file is a part of the Qarte software.
//
// \file	canApp.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 April 19
//-----------------------------------------------------------------------------


// NI headers
#include "canMainWindow.h"


// QT headers
#include <qapplication.h>
#include <qsplashscreen.h>


int	main( int argc, char** argv )
{
	// QT gui creation (Needed to allocate plugin qt components and application kernel)
	QApplication app( argc, argv );

	// Create main application form
	MainWindow mainWindow( &app );

	// Display main window and start processing events
	mainWindow.show( );
	app.connect( &app, SIGNAL( lastWindowClosed( ) ), &app, SLOT( quit( ) ) );

	// Process events
	return app.exec( );
}

