//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.h
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


#ifndef canMainWindow_h
#define canMainWindow_h


// Qanava headers
#include "../../src/qanGraph.h"
#include "../../src/qanGraphView.h"

#include "ui_canMainWindow.h"


// QT headers
#include <QMainWindow>
#include <QComboBox>


//-----------------------------------------------------------------------------
//!
/*!
	\nosubgrouping
*/
		class MainWindow : public QMainWindow, public Ui::MainWindow
		{
			Q_OBJECT

		public:

			MainWindow( QWidget* parent = 0 );

			virtual ~MainWindow( ) { }

		private:

			MainWindow( const MainWindow& );

			MainWindow& operator=( const MainWindow& );

			QComboBox*			_cbGridType;

			qan::GraphView*		_graphView;

		protected slots:

			void	gridChanged( int index );

			void	selectNode( qan::Node* node, QPoint p );
		};
//-----------------------------------------------------------------------------


#endif //

