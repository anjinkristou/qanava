//-----------------------------------------------------------------------------
// This file is a part of the Qanava software.
//
// \file	canMainWindow.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2005 November 11
//-----------------------------------------------------------------------------


// Qanava headers
#include "../../src/qanGraphView.h"
#include "../../src/qanGraphScene.h"
#include "../../src/qanGrid.h"
#include "./canMainWindow.h"


// QT headers
#include <QToolBar>
#include <QTreeView>
#include <QAction>
#include <QMessageBox>


using namespace qan;


//-----------------------------------------------------------------------------
MainWindow::MainWindow( QWidget* parent ) :
	QMainWindow( parent )
{
	setupUi( this );

    QHBoxLayout *hbox = new QHBoxLayout( _frame );
	hbox->setMargin( 0 );
	hbox->setSpacing( 2 );

	// Setup zoom and navigation toolbars
	QToolBar* zoomBar = addToolBar( "Zooming" );
	QSlider* sldZoom = new QSlider( Qt::Horizontal, zoomBar );
	sldZoom->setMinimum( 1 );
	sldZoom->setMaximum( 99 );
	sldZoom->setPageStep( 10 );
	sldZoom->setSliderPosition( 50 );
	sldZoom->setMinimumWidth( 190 );
	zoomBar->addWidget( sldZoom );

	QToolBar* navigationBar = addToolBar( "Navigation" );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	// Generate a simple graph
	Graph* graph = new Graph( );
	Node& na = *graph->addNode( "Node A<br> <b>Multi line</b> and rich-<br> text <i>HTML</i> label test.", 2 );
	Node& nb = *graph->addNode( "Node B", 1 );
	graph->addEdge( na, nb );

	Node& nc = *graph->addNode( "Node C<br>With an <b>icon</b>!", 1 );

	Node& nd = *graph->addNode( "<b>Node D</b><br>With a too long description.", 1 );


	// Setup item views
	_graphView = new qan::GraphView( _frame, QColor( 170, 171, 205 ) );
	GridItem* grid = new GridCheckBoardItem( _graphView, QColor( 255, 255, 255 ), QColor( 238, 230, 230 ) );
	hbox->addWidget( _graphView );

	// Configure node selection and tooltip popup rendering
//	connect( _graphView->getTooltipController( ), SIGNAL( nodeTipped(qan::Node*, QPoint) ), SLOT( showNodeTooltip(qan::Node*, QPoint) ) );
//	connect( _graphView->getSelectionController( ), SIGNAL( nodeSelected(qan::Node*, QPoint) ), SLOT( selectNode(qan::Node*, QPoint) ) );


	// Add canvas pan and zoom action to the navigation tobar
	_cbGridType = new QComboBox( navigationBar );
	_cbGridType->addItem( QIcon(), "<< Grid Type >>" );
	_cbGridType->addItem( QIcon(), "Regular" );
	_cbGridType->addItem( QIcon(), "Check Board" );
	connect( _cbGridType, SIGNAL(activated(int)), this, SLOT(gridChanged(int)));
	navigationBar->addWidget( _cbGridType );

	qan::GraphView* GraphView = _graphView;/*->getGraphView( );*/
	connect( sldZoom, SIGNAL( valueChanged(int) ), GraphView, SLOT( setZoomPercent(int) ) );
	navigationBar->setIconSize( QSize( 16, 16 ) );

	if ( GraphView->getAction( "zoom_in" ) != 0 )
		navigationBar->addAction( GraphView->getAction( "zoom_in" ) );
	if ( GraphView->getAction( "zoom_out" ) != 0 )
		navigationBar->addAction( GraphView->getAction( "zoom_out" ) );
	navigationBar->addSeparator( );
	if ( GraphView->getAction( "pan_controller" ) != 0 )
		navigationBar->addAction( GraphView->getAction( "pan_controller" ) );
	if ( GraphView->getAction( "zoom_window_controller" ) != 0 )
		navigationBar->addAction( GraphView->getAction( "zoom_window_controller" ) );

	QTreeView* treeView = new QTreeView( _frame );
	treeView->setAlternatingRowColors( true );
	treeView->setMaximumWidth( 100 );
	treeView->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
	hbox->addWidget( treeView );

	Style* blueNode = new Style( "bluenode" );
	blueNode->addImageName( "backimage", "images/gradblue.png" );
	blueNode->addColor( "bordercolor", 0, 0, 0 );
	graph->getM( ).applyStyle( &na, blueNode );

	Style* greenNode = new Style( "greennode" );
	greenNode->addImageName( "backimage", "images/gradgreen.png" );
	greenNode->addColor( "bordercolor", 50, 100, 200 );
	greenNode->addT< int >( "fontsize", 14 );
	graph->getM( ).applyStyle( &nb, greenNode );

	Style* iconNode = new Style( "iconnode" );
	iconNode->addImageName( "backimage", "images/gradblue.png" );
	iconNode->addImageName( "icon", "images/icon.png" );
	iconNode->addColor( "bordercolor", 0, 255, 0 );
	graph->getM( ).applyStyle( &nc, iconNode );

	Style* orangeNode = new Style( "orangenode" );
	orangeNode->addImageName( "backimage", "images/gradorange.png" );
	orangeNode->addColor( "bordercolor", 50, 100, 200 );
	orangeNode->addT< int >( "maximumwidth", 125 );
	orangeNode->addT< int >( "maximumheight", 25 );
	graph->getM( ).applyStyle( &nd, orangeNode );

	_graphView->setGraph( *graph );
	_graphView->layoutGraph( );
	graph->updateModels( );
	treeView->setModel( &graph->getO( ) );
}

void MainWindow::gridChanged( int index )
{

}

void MainWindow::selectNode( qan::Node * node, QPoint p )
{
	if ( node != 0 )
	{
		QString msg( "Node \"" );
		msg += node->getLabel( );
		msg += "\" selected.";
		QMessageBox::information( this, "Qanava Basic Test", msg );
	}
}
//-----------------------------------------------------------------------------


