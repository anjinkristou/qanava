//-----------------------------------------------------------------------------
// This file is a part of the Qarte software.
//
// \file	uiProgressQt.cpp
// \author	Benoit Autheman (benoit@libqanava.org)
// \date	2004 July 08
//-----------------------------------------------------------------------------


// Qarte headers
#include "uiProgressQt.h"


namespace qan { // ::qan
namespace qui { // ::qan::qui


/* Progress Constructor/Destructor *///----------------------------------------
ProgressQt::ProgressQt( QWidget* widget, int maxProgress ) :
	qan::utl::Progress( maxProgress ),
	_progress( 0 )
{
	// Setup progress dialog (Maximum is set to 100, since updateProgress() is called
	// directly in percents)
	_progress = new QProgressDialog( "Processing...", "Abort", 100,
									 widget, "progress", TRUE );
	_progress->setMinimumDuration( 0 );
	_progress->setProgress( 0 );
}

ProgressQt::~ProgressQt( )
{
	if ( _progress != 0 )
		delete _progress;
}
//-----------------------------------------------------------------------------



/* Views Management *///-------------------------------------------------------
bool	ProgressQt::getCancel( )
{
	if ( _progress != 0 )
		return _progress->wasCancelled( );
	return false;
}

void	ProgressQt::updateProgress( int percent )
{
	_progress->setProgress( percent );
}
//-----------------------------------------------------------------------------

} // ::qan::qui
} // ::qan

